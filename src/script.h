
#ifndef _SMEARGLE_SCRIPT_H
#define _SMEARGLE_SCRIPT_H

#include <stdbool.h>
#include <libgen.h>

#include "font.h"
#include "node.h"

typedef struct {
	char *name;
	char *filename;
	font_t font;
	char *format;
	unsigned int min_tiles_per_line;
	unsigned int max_tiles_per_line;
	bool leading_zeroes;
	unsigned int tile_offset;
	char *raw_filename;
	char *dedupe_filename;
	char *tilemap_filename;
	bool little_endian;
} script_t;

script_t script_create(toml_table_t *table, const char *base_path, node_t *fonts);
void script_destroy(script_t script);

#endif
