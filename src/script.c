
#include <toml.h>

#include "script.h"
#include "font.h"
#include "util.h"

script_t script_create(toml_table_t *table, const char *base_path, node_t *fonts) {
	script_t script;

#define TOML_GET_KEY(var, key, func) toml_datum_t (var) = func(table, (key));\
	if (!(var).ok) {\
		char str[200];\
		snprintf(str, 200, "key '%s' missing", (key)); \
		error(str, ""); \
	}
	TOML_GET_KEY(name, "name", toml_string_in)
	printf("  Processing script %s...\n", name.u.s);
	TOML_GET_KEY(filename, "filename", toml_string_in)
	TOML_GET_KEY(font_name, "font", toml_string_in)
	TOML_GET_KEY(min_tiles, "min_tiles_per_line", toml_int_in)
	TOML_GET_KEY(max_tiles, "max_tiles_per_line", toml_int_in)
	TOML_GET_KEY(tilemap_format, "tilemap_format", toml_string_in)
	TOML_GET_KEY(leading_zeroes, "leading_zeroes", toml_bool_in)
	TOML_GET_KEY(tile_offset, "tile_offset", toml_int_in)
	TOML_GET_KEY(raw_filename, "raw_filename", toml_string_in)
	TOML_GET_KEY(dedupe_filename, "dedupe_filename", toml_string_in)
	TOML_GET_KEY(tilemap_filename, "tilemap_filename", toml_string_in)
	TOML_GET_KEY(little_endian, "little_endian", toml_bool_in)
#undef TOML_GET_KEY

	font_t *font = font_find(fonts, font_name.u.s);
	if (font == NULL) {
		error("couldn't find font ", font_name.u.s);
	}

	script.filename = filename.u.s;
	script.font = *font;
	script.min_tiles_per_line = min_tiles.u.i;
	script.max_tiles_per_line = max_tiles.u.i;
	script.format = tilemap_format.u.s;
	script.leading_zeroes = leading_zeroes.u.b;
	script.tile_offset = tile_offset.u.i;
	script.raw_filename = raw_filename.u.s;
	script.dedupe_filename = dedupe_filename.u.s;
	script.tilemap_filename = tilemap_filename.u.s;
	script.little_endian = little_endian.u.b;

	return script;
}

void script_destroy(script_t script) {
	font_destroy(script.font);

	free(script.filename);
	free(script.format);
	free(script.raw_filename);
	free(script.dedupe_filename);
	free(script.tilemap_filename);
}
