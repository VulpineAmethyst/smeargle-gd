
#include <toml.h>

#include "util.h"
#include "node.h"
#include "font.h"
#include "script.h"

#define ERRBUFSZ 200

int main(int argc, char **argv) {
	if (argc < 2) {
		error("missing required argument: game.toml", "");
	}

	FILE *fp;
	char errbuf[ERRBUFSZ];
	fp = fopen(argv[1], "r");
	if (!fp) {
		snprintf(errbuf, 200, "can't open %s: ", argv[1]);
		error(errbuf, strerror(errno));
	}

	toml_table_t *conf = toml_parse_file(fp, errbuf, ERRBUFSZ);
	fclose(fp);

	if (!conf) {
		char str[ERRBUFSZ];
		snprintf(str, ERRBUFSZ, "can't parse %s: ", argv[1]);
		error(str, errbuf);
	}
	char *base_path = dirname(argv[1]);

	toml_datum_t name = toml_string_in(conf, "name");
	if (!name.ok) {
		error("missing key: name", "");
	}

	printf("Processing game %s...\n", name.u.s);
	puts("  Loading fonts...");
	toml_table_t *font = toml_table_in(conf, "font");
	if (!font) {
		error("couldn't find font table in ", argv[1]);
	}
	node_t *fonts = load_fonts(font, base_path);

	toml_array_t *scripts = toml_array_in(conf, "script");

	for (int i = 0; ; i++) {
		toml_table_t *toml_script = toml_table_at(scripts, i);
		script_t script = script_create(toml_script, base_path, fonts);
	}
}
