#ifndef _SMEARGLE_NODE_H
#define _SMEARGLE_NODE_H

typedef struct node_t {
	void *data;
	struct node_t *left, *right;
} node_t;

typedef int(node_comparator)(const node_t *a, const node_t *b);

node_t *node_create(void *data);
node_t *node_search(node_t *root, void *data, node_comparator func);
node_t *node_insert(node_t *node, void *data, node_comparator func);
node_t *node_find_leftmost(node_t *root);
node_t *node_remove(node_t *root, void *data, node_comparator func);

#endif