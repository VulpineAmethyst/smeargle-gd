#ifndef _SMEARGLE_FONT_H
#define _SMEARGLE_FONT_H

#include "node.h"

typedef struct map_t {
	const char *glyph;
	unsigned int index;
	unsigned int width;
	struct map_t *next;
} map_t;

typedef struct {
	const char *name;
	const char *image_filename;
	unsigned char bits_per_pixel;
	unsigned int width;
	unsigned int height;
	unsigned int map_entries;
	map_t *map;
} font_t;

extern void *fontdb;

node_t *load_fonts(toml_table_t *table, const char *base_path);
font_t *font_find(node_t *root, const char *key);
font_t font_create(const char *name, const char *filename);
void font_destroy(font_t font);
unsigned int map_create(toml_table_t *table, map_t **map);
void map_destroy(map_t *map);

#endif