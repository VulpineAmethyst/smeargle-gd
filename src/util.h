#ifndef _SMEARGLE_UTIL_H
#define _SMEARGLE_UTIL_H

#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

static void error(const char *msg, const char *msg1) {
	fprintf(stderr, "Error: %s%s\n", msg, msg1 ? msg1 : "");
	exit(1);
}

#endif
