# Smeargle

A tool for rendering text using bitmap images in PNG format, C version.

# Build Requirements

To build this project, you will need:

- libtoml (tomlc99); an embedded copy is available as a fallback if a system version isn't.
- libgd
- meson
- ninja

Building `smeargle`:

```sh
meson setup . output
meson compile
```

# License

This program is released under the MIT license; a copy of this license is provided in `LICENSE.md`.

